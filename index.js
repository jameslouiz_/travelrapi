var express = require('express');
var cors = require('cors');
var passport = require('passport');
var bodyParser = require("body-parser");
var app = express();
var multer = require("multer");
var mime = require("mime");
var db = require('mongoose');

db.connect('mongodb://localhost/travelr');
var crypto = require('crypto');

db.connection.on('error', console.error.bind(console, 'connection error:'));
db.connection.once('open', function() {
  console.log('connected');
});

app.engine('html', require('ejs').renderFile);
app.use(express.static(__dirname + '/public'));  

var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, './files/')
	},
	filename: function (req, file, cb) {
		req.body = JSON.parse(req.body.data);

		var current_date = (new Date()).valueOf().toString();
		var random = Math.random().toString();
		var hash = crypto.createHash('sha1').update(current_date + random).digest('hex');

		var fileName = hash;
		var extension = mime.extension(file.mimetype);
		var original = fileName + '.' + extension;

		req.imageProccessing = {};

     	req.imageProccessing.fileName = fileName;
     	req.imageProccessing.extension = extension;
     	req.imageProccessing.originalUrl = original;
     	req.imageProccessing.mimetype = file.mimetype;

     	cb(null, fileName + '.' + extension);
  }
});

var upload = multer({
	storage: storage
});

app.use(cors({
	origin: "http://localhost:3001",
	credentials: true
}));

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "http://localhost:3001");
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
	//res.header('Access-Control-Allow-Credentials', true);
	next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var strategies = {
	local: require('./auth/strategies/localStrategy.js'),
	facebook: require('./auth/strategies/facebookStrategy.js')
};

var auth = {
	middleware: {
		isLoggedIn: require('./auth/middleware/isLoggedIn.js'),
		isOwner: require('./auth/middleware/isOwner.js'),
		hasAccess: require('./auth/middleware/hasAccess.js')
	}
};

var schemas = require('./schemas.js')(db);

var models = {
	trip: db.model('Trip', schemas.trip(), 'Trips'),
	image: db.model('Image', schemas.image(), 'Images'),
	user: db.model('User', schemas.user(), 'Users')
};

// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
app.use(require('cookie-parser')());
app.use(require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false}));


// Initialize Passport and restore authentication state, if any, from the
// session.

strategies.local(models.user, passport);
strategies.facebook(models.user, passport);

app.use(passport.initialize());
app.use(passport.session());

var controllers = {
	images: require('./models/images.js')(models.trip, models.image),
	trips: require('./models/trips.js')(models.trip, models.image, models.user),
	users: require('./models/users.js')(models.user, passport)
};

app.get('/trips/all', controllers.trips.all);

app.get('/trips/:id', auth.middleware.hasAccess(models.trip), controllers.trips.getById);
app.get('/trips/:id/images', auth.middleware.hasAccess(models.trip), controllers.images.getByTripId);
app.post('/trips', auth.middleware.isLoggedIn, controllers.trips.insert);
app.put('/trips/:id', auth.middleware.isLoggedIn, auth.middleware.isOwner(models.trip), controllers.trips.update);
app.delete('/trips/:id', auth.middleware.isLoggedIn, auth.middleware.isOwner(models.trip), controllers.trips.destroy);

app.put('/images/:id', auth.middleware.isLoggedIn, auth.middleware.isOwner(models.image),  controllers.images.update);
app.get('/images/:id', /*auth.middleware.isLoggedIn, auth.middleware.isOwner(db, 'Images') ,*/  controllers.images.getById);
app.post('/images', upload.single('file'), auth.middleware.isLoggedIn, /*auth.middleware.isOwner(models.trip),*/ controllers.images.exif, controllers.images.uploadS3, controllers.images.insert);
app.delete('/images/:id', auth.middleware.isLoggedIn, auth.middleware.isOwner(models.image), controllers.images.destroy);

app.post('/users/signup', controllers.users.signup);
app.post('/users/login', controllers.users.login);
app.get('/users/isLoggedIn', controllers.users.isLoggedIn);
app.get('/users/logout', controllers.users.logout);
app.get('/users/me', controllers.users.me);
app.get('/users/me/trips', auth.middleware.isLoggedIn, controllers.trips.me);
app.get('/users/:id', controllers.users.getById);
app.put('/users/me', auth.middleware.isLoggedIn, controllers.users.update);
app.get('/users/:id/trips', /*auth.middleware.isLoggedIn,*/ controllers.trips.getByUserId);


// FACEBOOK
app.get('/users/login/facebook', passport.authenticate('facebook', { 
			display: 'popup',
			scope: [
				'public_profile', 'user_friends', 'email'
			]  
		}));

app.get('/users/login/facebook/callback', controllers.users.facebookCallback);

app.get('*', function (req, res) {
	res.sendfile('./public/index.html');
});


app.listen(3000);