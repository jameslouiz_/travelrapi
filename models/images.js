var uploader = require('s3-uploader');
var exif = require('exif-parser');
var fs = require('fs');

process.env.AWS_ACCESS_KEY_ID = 'AKIAI3F6OE4TUZWJ32ZA';
process.env.AWS_SECRET_ACCESS_KEY = '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';

module.exports = function (trip, image) {

	var module = {};

	var destroy = function (id) {
		var images = db.get('Images');

		images.remove({ _id: id }, function (err) {
			if (err) {
				res.send(err);
			} else {
				res.send({});
			}
		});
	};

	var getVersions = function (version) {
		var versions = {};

		versions.photo = [{
			maxHeight: 1040,
			maxWidth: 1040,
			format: 'jpg',
			suffix: '-large',
		},{
			maxWidth: 780,
			maxHeight: 780,
			suffix: '-medium'
		},{
			maxWidth: 320,
			aspect: '16:9!h',
			suffix: '-small'
		},{
			maxHeight: 300,
			aspect: '1:1',
			suffix: '-thumb_sq'
		}];

		versions.thumbnail = [{
			maxHeight: 350,
			aspect: '1:1',
			suffix: '-thumb_sq'
		},{
			maxWidth: 350,
			aspect: '2:1!h',
			suffix: '-thumb_rec'
		},{
			maxHeight: 150,
			aspect: '1:1',
			suffix: '-thumb_sm_sq'
		}];

		versions.avatar = [{
			maxHeight: 300,
			aspect: '1:1',
			suffix: '-thumb_sq'
		},{
			maxHeight: 50,
			aspect: '1:1',
			suffix: '-thumb_sm_sq'
		}]

		versions.cover = [{
			maxWidth: 1920,
			aspect: '4:1!h',
			suffix: '-large',
			quality: 60
		},{
			maxWidth: 640,
			aspect: '4:1!h',
			suffix: '-small',
			quality: 50
		}];

		return versions[version || 'photo'];
	};

	/**
	 * Handles the initial request for image and creates and object in the DB
	 * 
	 * @param  {Object} - Request object
	 * @param  {Object} - Response object
	 * @param  {Function}
	 * @return {Void}
	 */
	module.start = function (req, res, next) {
		var params = req.body,
			images = db.get('Images');

		images.insert({
			user: req.user._id,
			tripId: params.tripId
		}, function (err, doc) {
			if (err) {
				res.send(err);
			} else {
				res.send(doc);
			}
		});
	};

	module.exif = function (req, res, next) {
		fs.readFile(process.env.PWD + '/files/' + req.imageProccessing.originalUrl, function(err, buffer) {
			
			if (err) {
				return res.status(500).send(err);
			}

			var parser = exif.create(buffer);
			var result = parser.parse();

			req.imageProccessing.exif = result;

			next();
		});
	};

	module.uploadS3 = function (req, res, next) {

		var client = new uploader('travelr-api-s3', {
			aws: {
				region: 'us-east-1',
				acl: 'public-read',
				region: 'ap-southeast-1'
			},

			cleanup: {
				versions: true,
				original: true
			},

			original: {
				awsImageAcl: 'public-read'
			},

			versions: getVersions(req.body.kind)
		});

		client.upload(process.env.PWD + '/files/' + req.imageProccessing.originalUrl, {
			path: req.imageProccessing.fileName
		}, function(err, versions, meta) {

			if (err) {
				return res.status(500).send(err);
			}

			var v = {};

			versions.forEach(function (image) {

				if (image.original) {
					v['original'] = {
						width: image.width,
		    			height: image.height,
		    			key: 'original', 
		    			url: 'https://s3-ap-southeast-1.amazonaws.com/travelr-api-s3/' + image.key
					};
					return;
				}

				var key = image.suffix.split('-')[1];
				v[key] = {
					width: image.width,
	    			height: image.height,
	    			key: image.key, 
	    			url: 'https://s3-ap-southeast-1.amazonaws.com/travelr-api-s3/' + image.key
				};
			})

			req.imageProccessing.versions = v;

			next();
		});
	};


	/**
	 * Save new image variants to the initial image object in db
	 * 
	 * @param  {Object} - Request object
	 * @param  {Object} - Response object
	 * @param  {Function}
	 * @return {Void}
	 */
	module.insert = function (req, res, next) {
		var params = req.body,
			imageProccessing = req.imageProccessing;
			
		image.create({
			name: params.name,
			tripId: params.id,
            versions: imageProccessing.versions,
            geo: {
            	latitude: imageProccessing.exif.tags.GPSLatitude,
            	longitude: imageProccessing.exif.tags.GPSLongitude
            },
			kind: params.kind || 'photo'
		}, function (err, doc) {
			if (err) {
				res.send(err);
			} else {

				// Increment
				trip.update({ _id: params.tripId }, {
					$inc: {
						photoCount: 1
					}
				})
				.exec(function (err, res) {
					if (err) {
						return res.send(err);
					}
				})

				return res.send({
					photos: doc
				});
			}
		})
	};

	module.update = function (req, res, next) {
		var body = req.body,
			imageProccessing = req.imageProccessing;
			
		req.cursor.update({
			_id: req.params.id
		}, {
			'$set' : {
				name: body.name,
				description: body.description
			}
		}, function (err, doc) {
			if (err) {
				res.send(err);
			} else {
				image.findOne(doc._id, function (err, img) {
					if (err) {
						res.send(err);
					}
					
					res.send(img);
				});
			}
		})
	};

	/**
	 * Query images
	 * @param  {Object} - Request object
	 * @param  {Object} - Response object
	 * @return {Void}
	 */
	module.query = function (req, res) {
		var images = db.get('Images');

		var query = {
			tripId: req.query.trip
		};

		var defaults = {
			thumbnail: {
				$ne: true
			}
		};

		var params = (function (a, b){
		    for(var key in b) {
		        if(b.hasOwnProperty(key) && b[key]) {
		            a[key] = b[key];
		        }
		    }
		    return a;
		})(defaults, query);

		images.find(params, {}, function (err, doc) {
			if (err) {
				return res.send(err);
			}

			res.send(doc); 
		});
	};

	/**
	 * Get images by trip
	 * @param  {Object} req request object
	 * @param  {Object} res respsonse object
	 * @return {Void}   
	 */
	module.getByTripId = function (req, res) {
		
		req.cursor
		.deepPopulate('thumbnail user user.thumbnail')
		.exec(function (err, trip) {
			
			if (err) {
				return res.status(500).send(err);
			}

			image.find({tripId: trip._id.toString(), kind: 'photo'}).exec(function (err, photos) {
				
				if (err) {
					return res.status(500).send(err);
				}

				res.send({
					trips: trip,
					photos: photos
				});
			})
		});
	};


	module.getById = function (req, res) {

		image
		.findById(req.params.id)
		.exec(function (err, photos) {
			
			if (err) {
				return res.status(500).send(err);
			}

			res.send({
				photos: photos
			});
		});
	};


	module.destroy = function (req, res) {

		req.cursor.remove(function (err) {
			if (err) {
				res.send(err);
			} else {
				res.send({});
			}
		});
	};


	return module;
};