var async = require('async');
var Q = require('q');

module.exports = function (user, passport) {

	var module = {};

	module.getById = function (req, res) {
		var users = db.get('Users');

		users.findOne({
			_id: req.params
		}, function (err, doc) {
			if (err) {
				return res.send(err);
			}

			return res.send(doc);
		});
	};

	module.me = function (req, res) {

		//console.log(req.isAuthenticated());

		res.send(req.user);
	};

	module.signup = function (req, res, next) {
		var params = req.body,
			User = user;
		
		passport.authenticate('local-signup', function(err, user, info) {
	        if (err) { 
	        	return next(err); 
	        }	

	        if (user) {
	        	return res.send({message: 'User already exists'});
	        }

	        if (params.password !== params.confirmPassword) {
	        	return res.send({message: 'Passwords do not match'});
	        }

	        if (!user) {
	        	User.create({
	        		name: params.name,
	        		email: params.email,
	        		password: params.password,
	        		thumbnail: params.thumbnail
	        	}, function (err, user) {
	        		if (err) {
	        			return res.send(err);
	        		}

	        		req.login(user, function(err) {
	        			if (err) {
	        				return res.send(err);
	        			}
	        			
	        			return res.send(user);	
	        			
	        		});
	        	});
	        }

	    })(req, res, next);
	};

	module.login = function (req, res, next) {
		passport.authenticate('local', function(err, user, info) {
	        if (err) { 
	        	return res.send(err);
	        }	

	        if (!user) {
	        	return res.status(401).send({message: 'Incorrect credentials'});
	        }

	        req.login(user, function(err) {
				if (err) {
					return res.send(err);
				} 
				
				return res.send(user);	
			});

	    })(req, res, next);
	};

	module.logout = function (req, res, next) {
		req.logout();
    	res.send({});
	};

	module.isLoggedIn = function (req, res, next) {
		res.send({
			isAuthenticated: req.isAuthenticated()
		});
	};

	module.update = function (req, res, next) {
		var params = req.body;

		user.update({
			_id: req.user._id
		}, {
			'$set': {
				name: params.name,
				email: params.email,
				thumbnail: params.thumbnail,
				cover: params.cover,
				bio: params.bio
			}
		}, function (err, t) {
			if (err) {
				res.send(err);
			} else {
				
				user.findById(req.user._id)
				.populate('thumbnail cover')
				.select('_id thumbnail cover email name')
				.exec(function (err, user) {
					if (err) {
						return res.status(500).send(err);
					}

					return res.send(user);
				});

			}
		})
	};

	module.facebook = function (req, res, next) {
		passport.authenticate('facebook', { 
			display: 'popup',
			scope: [
				'public_profile', 'user_friends', 'email'
			]  
		});
	};

	module.facebookCallback = function (req, res, next) {
		passport.authenticate('facebook', function (err, user, info) {

			var state = 'success';

			req.login(user, function (err) {

				if (err) {
					state = 'error';
				}

				console.log(req.user);

				return res.render(process.env.PWD + '/auth/views/after-auth.html', { state: state, user: req.user ? req.user : null });

			})

		})(req, res, next);
	};

	return module;
};