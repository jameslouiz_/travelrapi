var async = require('async');
var Q = require('q');
var mongoose = require('mongoose');
var objectId = mongoose.Types.ObjectId;

module.exports = function (trip, image, user) {

	var module = {};



	// Get trip - // perform privacy check
	// Get trips - Query [Public] show only public trips
	// Get trips - By userID [All] - show all dependant on privacy checks
	// 
	// 
	// home
	//  - show all public trips
	//  - show all friends trips
	//  - show all my trips (including private private ones)
	//  
	//  Dashboard
	//  - show all my trips (including private ones)
	//  
	//  Trip
	//  - map
	//  	- get trip byID (check access level)
	// 	
	// 	- edit
	// 		- get trip byID (check access level)
	// 	
	// 	- 
	// 	
	// 	
	
	
	var getTrips = function (params, options, user) {
		var params = params || {},
			options = options || {},
			deferred = Q.defer(),
			user = user || {};

		params['$or'] = [
			{
				privacy: 1
			},
			{
				privacy: 2,
				user: user._id
			}
		];

		return trip.find(params);
	};

	/**
	 * Creates or updates an existing trip
	 * @param  {Object} - Request object
	 * @param  {Object} - Response object
	 * @return {Void}
	 */
	module.update = function (req, res) {
		var params = req.body;

		trip.update({
			_id: req.params.id
		}, {
			'$set': {
				name: params.name,
				description: params.description,
				thumbnail: params.thumbnail,
				location: params.location,
				geo: params.geo,
				privacy: parseInt(params.privacy, 10) || 1
			}
		}, function (err, t) {
			if (err) {
				res.send(err);
			} else {
				
				trip.findById(req.params.id)
				.populate('thumbnail')
				.exec(function (err, t) {
					if (err) {
						return res.status(500).send(err);
					}

					return res.send({
						trips: t
					});
				});

			}
		})
	};

	module.incImage = function (id, inc) {
		trip.update({
			_id: id
		}, {
			'$inc': {
				photos: inc
			}
		}, function (err, trip) {
			if (err) {
				res.send(err);
			} else {
				res.status(trip)
			}
		})
	};

	module.insert = function (req, res) {
		var params = req.body;

		trip.create({
			name: params.name,
			description: params.description,
			thumbnail: params.thumbnail,
			location: params.location,
			privacy: parseInt(params.privacy, 10) || 1,
			user: req.user._id,
			geo: params.geo
		}, function (err, t) {

			trip.findById(t._id)
			.populate('thumbnail')
			.exec(function (err, t) {
				if (err) {
					return res.status(500).send(err);
				}

				return res.send({
					trips: t
				});
			});

		})
	};

	module.insertTripAsBucket = function (req, res) {
		var params = req.body;

		trip.create({
			name: params.name,
			description: params.description,
			privacy: parseInt(params.privacy, 10) || 1,
			user: req.user._id,
			photos: [],
			isBucket: true
		}, function (err, doc) {
			if (err) {
				return res.status(500).send(err);
			}

			return res.send(doc);
		})
	};

	module.destroy = function (req, res) {

		trip.remove({ _id: req.params.id }, function (err) {
			if (err) {
				return res.send(err);
			}

			image.find({
				tripId: req.params.id
			})
			.exec(function (err, images) {

				if (err) {
					return res.send(err);
				}

				if (!images.length) {
					return res.send({});
				}

				images.forEach(function (img) {
					img.remove();
				})

				return res.send({});

			})
			
		});
	};

	module.getById = function (req, res) {

		req.cursor
		.populate('thumbnail')
		.populate('cover')
		.exec(function (err, trip) {
			
			if (err) {
				return res.status(500).send(err);
			}

			res.send(trip);
		});
	};

	module.getByUserId = function (req, res) {
		var params = { 
			user: objectId(req.params.id.toString())
		};

		var responseObject = {};

		user.findById(params.user)
		.select('_id name thumbnail cover')
		.populate('thumbnail cover')
		.exec(function (err, user) {

			if (err) {
				return res.status(500).send(err);
			}

			if (!user) {
				return res.status(404).send({});
			}

			responseObject.users = user;

			getTrips(params, {}, req.user)
			.populate('thumbnail')
			.sort('-createdOn')
			.exec(function (err, trips) {

				if (err) {
					return res.status(500).send(err);
				}

				responseObject.trips = trips;

				return res.status(200).send(responseObject);
			})
		})
	};

	module.me = function (req, res) {
		var params = { 
			user: objectId(req.user._id.toString())
		};

		req.user = req.user || {};

		var responseObject = {};

		user.findById(params.user)
		.select('_id name thumbnail cover')
		.populate('thumbnail cover')
		.exec(function (err, user) {

			if (err) {
				return res.status(500).send(err);
			}

			if (!user) {
				return res.status(404).send({});
			}

			responseObject.users = user;

			getTrips(params, {}, req.user)
			.populate('thumbnail')
			.sort('-createdOn')
			.exec(function (err, trips) {

				if (err) {
					return res.status(500).send(err);
				}

				responseObject.trips = trips;

				return res.status(200).send(responseObject);
			})
		})
	};

	module.all = function (req, res) {

		getTrips({}, {}, req.user)
		.deepPopulate('thumbnail user user.thumbnail')
		.sort('-createdOn')
		.exec(function (err, trips) {

			if (err) {
				return res.status(500).send(err);
			}

			return res.status(200).send({
				trips: trips
			});
		})
	};

	return module;
};