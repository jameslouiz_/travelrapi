'use strict';

/**
 * Common Index
 */
module.exports = angular.module('app.common', [
		require('./models').name,
		require('./interceptors').name,
		require('./components').name,
		require('./auth').name,
		require('./messages').name
	])
	.run(require('./init'));