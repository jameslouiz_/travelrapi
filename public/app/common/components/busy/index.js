 'use strict';

/**
 * Busy Directive Index
 */
module.exports = angular.module('app.common.components.busy', [])
	.directive('loading', require('./busyDirective'));
