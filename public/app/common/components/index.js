'use strict';

/**
 * Components Index
 */
module.exports = angular.module('app.common.components', [
		require('./mapDirective').name,
		require('./busy').name
	]);