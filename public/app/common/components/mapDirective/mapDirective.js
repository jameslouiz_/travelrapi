'use strict';

/**
 * Map Directive
 */

module.exports = ['tpMapFactory', '$q', function (tpMapFactory, $q) {
    return {
        restrict: 'A',
        scope: {
            center: '=',
            onInit: '&',
            onMove: '&'
        },
        controller: function ($scope) {

            /**
             * Flag to store controller this
             * @type {Object}
             */
            var Ctrl = this;

            /**
             * Flag to store the map object
             * @type {Object}
             */
            $scope.map = null;

            /**
             * Initialise the map
             * 
             * @param  {Object} element   Element element object to instantiate 
             * @param  {Number} latitude  Latitude to use as center
             * @param  {Number} longitude Longitude to use as center
             */
            Ctrl.init = function (element, latitude, longitude) {

                if (typeof window.google === 'undefined') {
                    return;
                }

                var options, map, deferred = $q.defer();

                options = {
                    zoom: 11,
                    minZoom: 4,
                    disableDefaultUI: true,
                    center: new google.maps.LatLng(latitude, longitude)
                };

                map = new google.maps.Map(element[0], options);

                // Pass map into markers factory
                tpMapFactory.setMap(map);

                $scope.map = map;

                Ctrl.bindEvents();
            };

            Ctrl.bindEvents = function () {
            
                var init = false;

                google.maps.event.addListener($scope.map, 'idle', function() {
                    // Run this the first time only
                    if (!init) {
                        $scope.onInit($scope.map);
                    } else {
                        $scope.onMove();
                        console.log('Map Move');
                    }

                    init = true;
                });
            }

        },
        link: function ($scope, element, attrs, Ctrl) {

            var center = $scope.center || {
                latitude: 0,
                longitude: 0
            };

            Ctrl.init(element, center.latitude, center.longitude);
            
        }
    };
}];