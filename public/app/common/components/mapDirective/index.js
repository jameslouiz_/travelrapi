 'use strict';

/**
 * Common Index
 */
module.exports = angular.module('app.common.components.map', [])
	.directive('tpMap', require('./mapDirective'))
	.factory('tpMapFactory', require('./mapFactory'));