    'use strict';

/**
 * Marker Factory to reliabley handle markers
 */
module.exports = ['$q', function ($q) {
    
	var exports = {};

	var map = null;

   	var markers = [];

    var markerIds = [];

    var bounds = [];

    var cluster = null;

    exports.$marker = {};

    exports.mapInit = {};

    /**
     * Private method to get marker by Id by looping the hash array
     * @param  {String} id Id of marker to get
     * @return {Object}    Object containing marker and index it was found
     */
    function getMarkerById (id) {
        var index = markerIds.indexOf(id);

        return {
            marker: markers[index],
            index: index
        };
    };

    function fitToMarkers(markers, map) {

        var bounds = new google.maps.LatLngBounds();

        // Create bounds from markers
        for( var index in markers ) {
            var latlng = markers[index].getPosition();
            bounds.extend(latlng);
        }

        // Don't zoom in too far on only one marker
        if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
           var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
           var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.01, bounds.getNorthEast().lng() - 0.01);
           bounds.extend(extendPoint1);
           bounds.extend(extendPoint2);
        }

        map.fitBounds(bounds);

        // Adjusting zoom here doesn't work :/

    }

    /**
     * Public method to pass int the map object
     * @param {google.maps.Map} mapObj map object to set
     * @return {Void}
     */
    exports.setMap = function (mapObj) {
        console.log('MAP');
        map = mapObj;
        cluster = new MarkerClusterer(map, [], {
            gridSize: 40,
            maxZoom: 12
        });

        if (angular.isFunction(exports.mapInit)) {
            exports.mapInit(map);
        }
    }

    /**
     * Add marker to map
     * @param {String} latitude  Latitude value to assign to marker
     * @param {String} longitude Longitude value to assign to marker
     * @param {String} id        Unique id to assign to maker
     */
    exports.addMarker = function (latitude, longitude, id, index) {
        
        if (typeof window.google === 'undefined') {
            return;
        }

        if (markerIds.indexOf(id) > -1) {
            return;
        }

        var marker = new google.maps.Marker({
            draggable: false,
            animation: false,
            position: new google.maps.LatLng(latitude, longitude),
            latitude: latitude,
            longitude: longitude,
            uid: id,
            index: index
        });

        markerIds.push(id);
        markers.push(marker);

        cluster.addMarker(marker);

        google.maps.event.addListener(marker, 'click', function() {
            //map.setZoom(8);
            //map.setCenter(marker.getPosition());
            if (angular.isFunction(exports.$marker.onClick)) {
                exports.$marker.onClick(marker);
            }
        });
    };

    /**
     * Remove a single marker from map
     * @param  {String} id Id of marker to remove
     * @return {Void}    
     */
    exports.remove = function (id) {
        var marker = getMarkerById(id),
            markerObj = marker.marker,
            markerIndex = marker.index;

        markerIds.splice(markerIndex, 1);
        markers.splice(markerIndex, 1);
       
    };

    /**
     * Utility method to clear the map
     * @return {Void} 
     */
    exports.removeAll = function () {
        var test = markerIds;

        if (!test.length) {
            return
        };

        var i = test.length;
        while (i--) {
            exports.remove(test[i]);
        }

        cluster.clearMarkers();
    };

    /**
     * Clear Markers not in view
     * @return {[type]} [description]
     */
    exports.removeOutOfBounds = function () {
        for (var i = 0; i < markers.length; i++) {
            var marker = markers[i];
            if (!map.getBounds().contains(marker.getPosition()) && marker.getVisible()) {
                exports.remove(marker.uid);
            }
        }
    };

    /**
     * Remove markers that are not in the collection
     * so no need to remove all and replace with collection
     * @param  {Array} collection Array of locations
     * @return {Void}            
     */
    exports.removeIrrelevant = function (collection) {
        
        var collectionHash = [];
        for (var a = 0; a < collection.length; a++) {
            collectionHash.push(collection[a].id);
        }

        // Return all elements in A, unless in B
        var arr = markerIds.filter(function(i) {return collectionHash.indexOf(i) < 0;});

        var i = arr.length;
        while (i--) {
            exports.remove(arr[i]);
        }
    }

    /**
     * Add a collection of locations to map
     * @param {Array} collection Array of locations to add
     */
    exports.addCollection = function (collection) {

        if (typeof window.google === 'undefined') {
            return;
        }

        var bounds = new google.maps.LatLngBounds();

        if (!(collection || []).length) {
            return;
        }

        for (var i = 0; i < collection.length; i++) {
            var location = collection[i],
                latitudeInvalid,
                longitudeInvalid;

            location.geo = location.geo || {
                latitude: 0,
                longitude: 0
            };

            latitudeInvalid = location.geo.latitude === 0 || location.geo.latitude === null;
            longitudeInvalid = location.geo.longitude === 0 || location.geo.longitude === null;

            if (latitudeInvalid && longitudeInvalid) {
                continue;
            }
            
            exports.addMarker(location.geo.latitude, location.geo.longitude, location._id, i);
            bounds.extend(new google.maps.LatLng(location.latitude,location.longitude));

        }

        fitToMarkers(markers, map);
    };

    /**
     * Get bounds of map viewport and return flat object of NE/SW lat lng
     * @return {Object} Object of lat/lng pairs of NE/SW corners of viewport
     */
    exports.getBounds = function () {
        var bounds = map.getBounds();

        return {
            neLatitude: bounds.getNorthEast().lat(),
            neLongitude: bounds.getNorthEast().lng(),
            swLatitude: bounds.getSouthWest().lat(),
            swLongitude: bounds.getSouthWest().lng()
        };
    };

    exports.centerToMarker = function (markerId) {

        if (typeof window.google === 'undefined') {
            return;
        }   

        var marker = getMarkerById(markerId),
            position = new google.maps.LatLng(marker.marker.latitude, marker.marker.longitude);
        
        map.panTo(position);
    };

    exports.fitMapToMarkers = function () {
        if (!markers.length || !map === null) {
            return;
        }
        
        fitToMarkers(markers, map);
    };

    return exports;
}];