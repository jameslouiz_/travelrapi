'use strict';

/**
 * Photos Model
 * @return {object} Returns the public methods
 */
module.exports = [function () {
	
	var exports = {
			DS: {
				trips: {
					collection: [],
					active: {}
				},
				photos: {
					collection: [],
					active: {}
				},
				users: {
					collection: [],
					active: {}
				}
			}
		};

	exports.setStatus = function (status, collectionName) {
		exports.status[collectionName] = status;
	};

	exports.getStatus = function (status, collectionName) {
		return exports.status[collectionName];
	};

	exports.setActive = function (object, collectionName) {
		exports.DS[collectionName].active = object;
	};

	exports.getFromCollectionById = function (id, collectionName) {
		return exports.DS[collectionName].collection.filter(function( obj ) {
	        return obj._id === id;
	    })[0];
	};

	exports.getIndexFromCollectionById = function (id, collectionName) {
		return exports.DS[collectionName].collection.indexOf(exports.getFromCollectionById(id, collectionName));
	};

	exports.removeFromCollectionById = function (id, collectionName) {
		exports.DS[collectionName].collection.splice(exports.getIndexFromCollectionById(id, collectionName), 1);
	};

	exports.getIndexFromCollectionById = function (id, collectionName) {
		var array = exports.DS[collectionName].collection;

		for (var i = 0; i < array.length; i++) {
	        if (array[i]['_id'] === id) {
	            return i;
	        }
	    }
	    return -1;
	};

	exports.pushToCollection = function (object, collectionName) {

		if (!object || !object._id) return;

		var index = exports.getIndexFromCollectionById(object._id, collectionName);

		if (index > -1) {
			exports.DS[collectionName].collection[index] = object;
		} else {
			exports.DS[collectionName].collection.push(object);
		}
	};

	exports.resetCollection = function (collectionName) {
		exports.DS[collectionName].collection = [];
	};

	exports.initCollection = function (array, collectionName) {
		
		exports.resetCollection(collectionName);

		for (var i = 0; i < array.length; i++) {
			exports.pushToCollection(array[i], collectionName);
		}
	};

	return exports;

}];