'use strict';

/**
 * Trips Model
 * @return {object} Returns the public methods
 */
module.exports = ['$http', '$q', 'photosModel', 'sharedModel', function ($http, $q, photosModel, sharedModel) {
	
	var exports = {},
		host = 'http://localhost:3000';

	/**
	 * Get all public trips by user ID
	 * @param  {String} id Unique ID of user
	 * @return {Promise}  
	 */
	exports.getTripsByUser = function (id) {
		return $http({
			method: 'GET',
			url: host + '/users/' + id + '/trips',
			withCredentials: true,
			secure: true,
			notifyEmptyResults: true
		}).then(function (res) {
			
			sharedModel.setActive(res.data.users, 'users');
			sharedModel.initCollection(res.data.trips, 'trips');

			return sharedModel.DS;
		})
	};

	/**
	 * Get trip by trip Id
	 * @param  {String} id Unique ID of user
	 * @return {Promise}  
	 */
	exports.getTripById = function (id) {
		return $http({
			method: 'GET',
			url: host + '/trips/' + id,
			withCredentials: true,
			notifyEmptyResults: true
		}).then(function (trip) {

			sharedModel.setActive(trips.data.trips, 'trip');
			//sharedModel.setActive(trips.data.trips, 'trip');

			return sharedModel.DS;
		})
	};

	/**
	 * Save the 
	 * @param  {[type]} id [description]
	 * @return {[type]}    [description]
	 */
	exports.save = function (model) {
		var method = model._id ? 'PUT' : 'POST',
			suffix = model._id ? '/' + model._id : '',
			promise = $q.when({ data: { photos: { _id: model.thumbnail }}});

		// if thumbnail changed
		if (model.file) {
			promise = photosModel.uploadOfKind(model.file, 'thumbnail');
		}

		return promise
			.then(function (photo) {
				var dummy = {
					data: {
						photos: {
							_id: model.thumbnail
						}
					}
				}

				var p = photo || dummy;

				model.file = undefined;

				return $http({
					method: method,
					url: host + '/trips' + suffix,
					withCredentials: true,
					secure: true,
					data: {
						name: model.name,
						description: model.description,
						location: model.location,
						privacy: model.privacy,
						thumbnail: p.data.photos._id,
						geo: model.geo
					}
				})

			})
			.then(function (res) {

				sharedModel.setActive(res.data.trips, 'trips');

				sharedModel.pushToCollection(res.data.trips, 'trips');

				return $q.when(sharedModel.DS);

			}, function (res) {
				return $q.reject('error');
			})

	};

	exports.destroy = function (id) {
		return $http({
			method: 'DELETE',
			url: host + '/trips/' + id,
			withCredentials: true,
			confirm: 'Are you sure you want to delete this trip?',
			secure: true
		})
		.then(function () {
			sharedModel.removeFromCollectionById(id, 'trips');
		})
	};



	exports.all = function () {
		return $http({
			method: 'GET',
			url: host + '/trips/all',
			withCredentials: true
		}).then(function (res) {
			
			sharedModel.initCollection(res.data.trips, 'trips');

			return sharedModel.DS;
		}, function (res) {
			return $q.reject(res);
		})
	};


	return exports;

}];