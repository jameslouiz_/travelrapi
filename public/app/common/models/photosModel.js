'use strict';

/**
 * Photos Model
 * @return {object} Returns the public methods
 */
module.exports = ['$http', '$q', 'Upload', 'sharedModel', function ($http, $q, Upload, sharedModel) {
	
	var exports = {},
		host = 'http://localhost:3000';


	function upload (file, data) {
		return Upload.upload({
			method: 'POST',
			url: host + '/images',
			data: data,
			file: file,
			withCredentials: true,
			secure: true
		});
	};

	/**
	 * Get all public trips by user ID
	 * @param  {String} id - Unique ID of user
	 * @return {Promise}  
	 */
	exports.getPhotosByTrip = function (id) {
		return $http({
			method: 'GET',
			url: host + '/trips/' + id + '/images',
			withCredentials: true,
			notifyEmptyResults: true
		})
		.then(function (res) {

			sharedModel.setActive(res.data.trips, 'trips');
			sharedModel.initCollection(res.data.photos, 'photos');

			return sharedModel.DS;
		})
	};

	/**
	 * Get single photo by ID
	 * @param  {String} id - Id of photo to return
	 * @return {Promise} 
	 */
	exports.getPhotoById = function (id) {
		return $http({
			method: 'GET',
			url: host + '/images/' + id,
			withCredentials: true,
			notifyEmptyResults: true
		})
		.then(function (res) {
			sharedModel.setActive(res.data.photos, 'photos');
			
			return sharedModel.DS;
		})
	};

	exports.upload = function (id, file, thumbnail) {

		var data = {
				name: file.name,
				tripId: id,
				kind: 'photo'
			};

		return upload (file, data).then(function (res) {
			sharedModel.pushToCollection(res.data.photos, 'photos');

			return sharedModel.DS;
		}, function () {
			$q.reject();
		});
	};

	exports.uploadOfKind = function (file, kind) {
		var data = {
				name: file.name,
				kind: kind
			};


		return upload (file, data);
	};

	exports.update = function (model) {
		return $http({
			method: 'PUT',
			url: host + '/images/' + model._id,
			data: {
				name: model.name,
				description: model.description
			},
			withCredentials: true
		})
	};

	exports.destroy = function (id) {
		return $http({
			method: 'DELETE',
			url: host + '/images/' + id,
			withCredentials: true,
			confirm: 'Are you sure you want to delete this photo?'
		})
		.then(function () {
			sharedModel.removeFromCollectionById(id, 'photos');
		})
	};

	return exports;

}];