'use strict';

/**
 * Models Index
 */
module.exports = angular.module('app.common.models', [
		require('./vendor').name
	])
	.factory('sharedModel', require('./sharedModel'))
	.factory('tripsModel', require('./tripsModel'))
	.factory('photosModel', require('./photosModel'));