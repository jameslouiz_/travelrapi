'use strict';

/**
 * Google API requests
 * @return {object} Returns the public methods
 */
module.exports = ['$http', '$q', function ($http, $q) {
	
	var exports = {};

	exports.getLocation = function (val) {
		return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
			params: {
				address: val,
				sensor: false
			}
		}).then(function(response){
			return response.data.results.map(function(item){
				return item.formatted_address;
			});
		});
	};

	/**
     * Geocode a location to latitude and longitude from an address string
     * @param  {String} location Plain text string of an address
     * @return {Object}          Promise  
     */
    exports.toLatLong = function (location) {
        
        var deferred = $q.defer();

        if (typeof window.google === 'undefined') {
            deferred.resolve();
            return deferred.promise;
        }

        var geocoder = new google.maps.Geocoder();
        
        // Make geocode request
        geocoder.geocode({ 'address': location }, function(results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat(),
                    longitude = results[0].geometry.location.lng();

                deferred.resolve({latitude: latitude, longitude: longitude});
            } else {
                deferred.reject(results);
            }

        }); 

        return deferred.promise;

    };

    /**
     * Geocode to an address string from latitude and longitude
     * @param  {Number} latitude  Latitude coordinate
     * @param  {Number} longitude Longitude coordinate
     * @return {Object}           Promise
     */
    exports.toLocation = function (latitude, longitude) {

        var deferred = $q.defer();
            
        if (typeof window.google === 'undefined') {
            deferred.resolve();
            return deferred.promise;
        }

        var latLng = new google.maps.LatLng(latitude, longitude),
            geocoder = new google.maps.Geocoder();

        
        // Make geocode request
        geocoder.geocode({ 'latLng': latLng }, function(results, status) {
            
            if (status == google.maps.GeocoderStatus.OK) {
                deferred.resolve(results);
            } else {
                deferred.reject(results);
            }

        }); 

        return deferred.promise;
    };

	return exports;

}];