'use strict';

/**
 * Vendor Models Index
 */
module.exports = angular.module('app.common.models.vendor', [])
	.factory('googleAPI', require('./googleAPI'))