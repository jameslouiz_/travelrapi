'use strict';

/**
 * Core Run Block
 */
module.exports = ['$rootScope', 'cfpLoadingBar',
    function($rootScope, cfpLoadingBar) {
    
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        cfpLoadingBar.start();
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        cfpLoadingBar.complete();
    });

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams) {
        cfpLoadingBar.complete();
    });

}];