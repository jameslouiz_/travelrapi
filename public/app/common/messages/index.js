'use strict';

/**
 * Messages Index
 */
module.exports = angular.module('app.common.messages', [])
	.controller('MessageModalsController', require('./messageModalsController'))
	.factory('messageModals', require('./messageModals'));