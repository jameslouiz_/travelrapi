'use strict';

/**
 * Basic prompt controller
 */
module.exports = ['$scope', '$modalInstance', 'prompt', function ($scope, $modalInstance, prompt) {

    /**
     * Flag to make message available to scope
     * @type {String}
     */
    $scope.message = prompt;

    /**
     * Close method to close modal
     * @return {Void} 
     */
    $scope.close = function () {
        $modalInstance.close('OK');
    };

    $scope.dismiss = function () {
        $modalInstance.dismiss('cancel');
    };

}];