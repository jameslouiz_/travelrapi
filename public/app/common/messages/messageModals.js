'use strict';

/**
 * Message Modals
 * @return {object} Returns the public methods
 */
module.exports = ['$modal', function ($modal) {
	
	var exports = {};

	/**
     * Opens and creates a modal instance
     * @param  {String} heading Heading Text
     * @param  {String} body    Body text
     * @param  {String} type    Concats to modal- to change style
     * @return {Void} 
     */
    var open = function (heading, body, type) {

        var modalInstance = $modal.open({
            templateUrl: './app/common/messages/messageModal.html',
            controller: 'MessageModalsController',
            windowClass: 'modal-'+type,
            resolve: {
                prompt: function () {
                    return {
                        heading: heading,
                        body: body
                    };
                }
            }
        });
    };

    exports.confirm = function (message) {
        var modalInstance = $modal.open({
            templateUrl: './app/common/messages/confirmModal.html',
            controller: 'MessageModalsController',
            size: 'sm',
            resolve: {
                prompt: function () {
                    return {
                        body: message
                    };
                }
            }
        });

        return modalInstance;
    };

    /**
     * Wrapper for error prompt 
     * @param  {String} message Message to display in prompt
     * @return {Void}     
     */
    exports.error = function (message) {
        var heading = 'Error!'

        open(heading, message, 'danger');
    };

    /**
     * Wrapper for warning prompt
     * @param  {String} message Message to display in prompt
     * @return {Void}
     */
    exports.warning = function (message) {
        var heading = 'Warning!';

        open(heading, message, 'warning');
    };

    /**
     * Wrapper for success prompt
     * @param  {String} message Message to display in prompt
     * @return {Void}        
     */
    exports.success = function (message) {
        var heading = 'Success!';

        open(heading, message, 'success');
    };

    /**
     * Wrapper for info prompt
     * @param  {String} message Message to display in prompt
     * @return {Void}      
     */
    exports.info = function (message) {
        var heading = 'Info!';

        open(heading, message, 'info');
    };

	return exports;

}];