'use strict';

/**
 * Initialise HTTP interceptors
 */
module.exports = ['$httpProvider', function($httpProvider) {

	$httpProvider.interceptors.push('status404HttpInterceptor');
 	$httpProvider.interceptors.push('confirmActionHttpInterceptor');

}];