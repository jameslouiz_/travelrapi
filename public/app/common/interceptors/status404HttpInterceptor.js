'use strict';

/**
 * Auth HTTP interceptor
 */
module.exports = ['$q', '$injector', function($q, $injector) {

	return {
        response: function(response) {
        	var $state = $injector.get('$state');

        	if (response.config.notifyEmptyResults && angular.equals({}, response.data)) {
                
                $state.go('app.public.error.status', {statusCode: 404})

                return $q.reject(response);
        	}
        	
            return response || $q.when(response);
        }
    };
}];