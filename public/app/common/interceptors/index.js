'use strict';


module.exports = angular.module('app.common.intereptors', [])
	.factory('status404HttpInterceptor', require('./status404HttpInterceptor'))
	.factory('confirmActionHttpInterceptor', require('./confirmActionHttpInterceptor'))
	.config(require('./config'));