'use strict';

/**
 * Confirm HTTP interceptor
 */
module.exports = ['$q', '$injector', function($q, $injector) {

	return {
        request: function(config) {

        	if (config.confirm) {

                var messageModals = $injector.get('messageModals');

                return messageModals.confirm(config.confirm).result
                    .then(function () {
                        return config;
                    }, function () {
                        return $q.reject(config);
                    })
                 
        	} else {
                return config; 
            }
        }
    };
}];