'use strict';

/**
 * Auth Config
 */
module.exports = ['$httpProvider', function($httpProvider) {

   	$httpProvider.interceptors.push('authHttpInterceptor');

}];