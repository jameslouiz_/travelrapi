'use strict';

/**
 * Auth Index
 */
module.exports = angular.module('app.common.auth', [])
	.factory('userModal', require('./userModal'))
	.factory('userModel', require('./userModel'))
	.factory('authHttpInterceptor', require('./authHttpInterceptor'))
	.controller('UserModalController', require('./userModalController'))
	.config(require('./config'))
	.run(require('./init'));