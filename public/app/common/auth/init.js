'use strict';

/**
 * Core Run Block
 */
module.exports = ['$rootScope', '$state', '$stateParams', 'userModel', 'userModal', 'cfpLoadingBar',
    function($rootScope, $state, $stateParams, userModel, userModal, cfpLoadingBar) {


    
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (userModel.bypass) return;


        toState.data = toState.data || {};

        if (!toState.data.secure) {
            return;
        }

        event.preventDefault();
        
        userModel.isLoggedIn().then(function (isLoggedIn) {
            console.log('hes here');
            userModel.bypass = true; 
            console.log(isLoggedIn);
            if (isLoggedIn) {  
                console.log('logged in');
                $state.go(toState, toParams); 
            } else {
                console.log('not logged in');
                cfpLoadingBar.complete();
                var state = fromState.name !== '' ? fromState.name : 'app.public.home'; // to do, go to some error page
                userModal.authenticate().result
                    .then(function (user) {
                        // success so proceed
                        $state.go(toState, toParams);
                    }, function () {
                        // failure so cancel
                        $state.go(state, toParams); 
                    });
            }
        })

    });
}];