'use strict';

/**
 * User Auth Controller
 */
module.exports = ['$scope', '$state', 'userModel', '$modalInstance', '$window', function ($scope, $state, userModel, $modalInstance, $window) {

	$window.oauthSuccess = function (user) {
		userModel.setUser(user);
		$modalInstance.dismiss('close');
	}

	$scope.showLogin = true;

	$scope.submit = function (model) {
		var promise = $scope.showLogin ? userModel.login(model) : userModel.register(model);

		promise.then(function (res) {
			
			$modalInstance.close(res.data);

		}, function (err) {
			console.log(err, 'err');
		})
	};

	$scope.facebook = function () {
		$window.open('http://localhost:3000/users/login/facebook', '', 'width=600, height=400');
	}

	$scope.close = $modalInstance.dismiss;

}];