'use strict';

/**
 * Auth HTTP interceptor
 */
module.exports = ['$q', '$injector',
    function($q, $injector) {

	return {
        responseError: function(response) {
            var userModal,
                messageModals;

        	if (response.config.secure && response.status === 401) {
                userModal = $injector.get('userModal');
        		userModal.authenticate();
        	}

            if (response.config.secure && response.status === 403) {
                messageModals = $injector.get('messageModals');
                messageModals.error('You do not have permission to perform this action.');
            }
        	
            return $q.reject(response);
        }
    };
}];