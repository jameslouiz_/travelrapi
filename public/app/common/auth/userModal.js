'use strict';

/**
 * Message Modals
 * @return {object} Returns the public methods
 */
module.exports = ['$modal', function ($modal) {
	
	var exports = {};

	/**
     * Opens and creates a modal instance
     * @param  {String} heading Heading Text
     * @param  {String} body    Body text
     * @param  {String} type    Concats to modal- to change style
     * @return {Void} 
     */
    var open = function (heading, body, type) {

        var modalInstance = $modal.open({
            templateUrl: './app/common/auth/userModal.html',
            controller: 'UserModalController',
            size: 'sm'
        });

        return modalInstance;
    };

    exports.authenticate = function () {
        return open();
    };

	return exports;

}];