'use strict';

/**
 * Users Model
 * @return {object} Returns the public methods
 */
module.exports = ['$http', '$q', '$rootScope', 'photosModel', 'sharedModel', function ($http, $q, $rootScope, photosModel, sharedModel) {
	
	var exports = {};

	$rootScope.user = {};

	var setUser = function (user) {
		$rootScope.user = user;
		//sharedModel.setActive(user, 'users');
	};

	var getUser = function () {
		return $http({
			method: 'GET',
			url: 'http://localhost:3000/users/me',
			withCredentials: true
		}).then(function (res) {
			setUser(res.data);
		})
	};

	exports.getUser = getUser;
	exports.setUser = setUser;

	exports.isLoggedIn = function (model) {
		return $http({
			method: 'GET',
			url: 'http://localhost:3000/users/isLoggedIn',
			withCredentials: true
		}).then(function (res) {
			return res.data.isAuthenticated;
		})
	};

	exports.register = function (model) {
		console.log(model);
		return $http({
			method: 'POST',
			url: 'http://localhost:3000/users/signup',
			data: {
				email: model.email,
				password: model.password,
				confirmPassword: model.confirmPassword,
				name: model.name,
			},
			withCredentials: true
		}).then(function (res) {
			setUser(res.data);
			return res;
		})
	};

	exports.login = function (model) {
		return $http({
			method: 'POST',
			url: 'http://localhost:3000/users/login',
			data: {
				email: model.email,
				password: model.password
			},
			withCredentials: true
		}).then(function (res) {
			setUser(res.data);
			return res;
		})
	};

	exports.logout = function (model) {
		return $http({
			method: 'GET',
			url: 'http://localhost:3000/users/logout',
			withCredentials: true,
			confirm: 'Are you sure you want to log out?'
		}).then(function (res) {
			setUser({});
			exports.bypass = false;
		})
	};

	exports.save = function (model) {
		var thumbPromise = $q.when({ data: { photos: { _id: model.thumbnail }}});
		var coverPromise = $q.when({ data: { photos: { _id: model.cover }}});

		// if thumbnail changed
		if (model.thumb) {
			thumbPromise = photosModel.uploadOfKind(model.thumb, 'avatar');
		}

		if (model.coverPic) {
			coverPromise = photosModel.uploadOfKind(model.coverPic, 'cover');
		}

		return $q.all([thumbPromise, coverPromise]).then(function (photos) {

			model.thumbnail = photos[0].data.photos._id;
			model.cover = photos[1].data.photos._id;

			return $http({
				method: 'PUT',
				url: 'http://localhost:3000/users/me',
				withCredentials: true,
				data: model
			})
		})
		.then(function (res) {
			setUser(res.data);
			return $q.when(res);
		}, function (res) {
			return $q.reject(res)
		})
	};

	exports.getUser();

	return exports;

}];