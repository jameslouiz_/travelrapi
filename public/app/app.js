'use strict';

/**
 * Main App File
 */

// Require the dependancies
require('angular'); 
require('angular-ui-router');
require('angular-bootstrap');
require('angular-touch');
require('angular-loading-bar');
require('ng-file-upload');


angular
	.module('app', [
		'ui.router',
		'ui.bootstrap',
		'cfp.loadingBar',
		//'cfp.loadingBarInterceptor',
		'ngTouch',
		'ngFileUpload',

        require('./views').name,
        require('./common').name
	])


angular.element(document).ready(function() {
	angular.bootstrap(document, ['app']);
});