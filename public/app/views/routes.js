
'use strict';

/**
 * Pages Routes
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('app', {
        abstract: true,
        views: {
            'header': {
                templateUrl: './app/views/layout/header/header.html',
                controller: 'HeaderController'
            },
            'main': {
                template: '<div ui-view class="main"></div>'
            }
        }
    })
    .state('app.public', {
        abstract: true,
        //template: '<ui-view></ui-view>'
        views: {
            '': {
                template: '<div ui-view class="main"></div>'
            }
        }
    })
    .state('app.secure', {
        abstract: true,
        views: {
            '': {
                template: '<div ui-view></div>'
            }
        },
        data: {
            secure: true
        }
        //templateUrl: './app/views/index.html'
    })


    $urlRouterProvider.otherwise('/');

}];