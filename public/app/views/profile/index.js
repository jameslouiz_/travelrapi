'use strict';

/**
 * Profile Index
 */

module.exports = angular.module('app.views.profile', [
		require('./view').name,
		require('./inspector').name
	]);