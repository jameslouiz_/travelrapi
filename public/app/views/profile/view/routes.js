
'use strict';

/**
 * Pages Routes
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('app.public.profile', {
        url: '/user/:id',
        views: {
            '': {
                templateUrl: './app/views/profile/view/profile.view.html',
                controller: 'ProfileViewController'
            }
        },
        resolve: {
            model: ['$stateParams', 'tripsModel', function ($stateParams, tripsModel) {
                return tripsModel.getTripsByUser($stateParams.id);
            }]
        }
    })
    .state('app.secure.profile', {
        url: '/me',
        views: {
            '': {
                templateUrl: './app/views/profile/view/profile.view.me.html',
                controller: 'ProfileViewController'
            }
        },
        resolve: {
            model: ['$stateParams', 'tripsModel', function ($stateParams, tripsModel) {
                return tripsModel.getTripsByUser('me');
            }],
            photos: ['$stateParams', 'photosModel', function ($stateParams, photosModel) {
                return photosModel.getPhotosByTrip('568c9a4f68c7a5bc181066d3');
            }]
        }
    });


    $urlRouterProvider.otherwise('/');

}];