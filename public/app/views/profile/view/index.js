'use strict';

/**
 * Public Profile Index
 */
module.exports = angular.module('app.views.profile.view', [])
	.controller('ProfileViewController', require('./profileViewController'))
	.config(require('./routes'));