'use strict';

/**
 * Profile Controller
 */
module.exports = ['$scope', 'sharedModel', function ($scope, sharedModel) {

	$scope.model = sharedModel.DS;

}];