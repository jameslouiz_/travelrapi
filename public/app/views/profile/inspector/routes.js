
'use strict';

/**
 * Pages Routes
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    var modalOnEnter = function (isNew) {
        return ['$state', 'profileInspectorModal', 'sharedModel', function($state, profileInspectorModal, sharedModel) {
            var model = sharedModel.DS.users.active;
            var modal = profileInspectorModal(model);

            modal.result.then(function(res) {
                if (res === '!^') return;

                $state.go('^');
            }, 
            function(res) {
                if (res === '!^') return;

                $state.go('^');
            });
        
        }]
    };

    $stateProvider
    .state('app.secure.profile.edit', {
        url: '/edit',
        onEnter: modalOnEnter()
    });


    $urlRouterProvider.otherwise('/');

}];