'use strict';

/**
 * Profile Inspector Modal Definition
 *
 */
module.exports = ['$modal', function ($modal) {
    return function (model) {
        var modalInstance = $modal.open({
            templateUrl: './app/views/profile/inspector/profile.inspector.html',
            //template: '<div ui-view="modal"></div>'
            controller: 'ProfileInspectorController',
            size: 'md',
            resolve: {
                model: function () {
                    return model;
                }
            }
        });

        return modalInstance;
    };
}];