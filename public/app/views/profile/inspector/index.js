'use strict';

/**
 * Profile Inspector Index
 */
module.exports = angular.module('app.views.profile.inspector', [])
	.factory('profileInspectorModal', require('./profileInspectorModal'))
	.controller('ProfileInspectorController', require('./profileInspectorController'))
	.config(require('./routes'));