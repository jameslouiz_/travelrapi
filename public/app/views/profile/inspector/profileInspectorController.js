'use strict';

/**
 * Profile Inspector Controller
 */
module.exports = ['$scope', 'userModel', '$modalInstance', function ($scope, userModel, $modalInstance) {

	$scope.inspector = angular.copy($scope.user);
	
	$scope.save = function (model) {
		$scope.Promise = userModel.save(model).then(function (res) {
			$scope.inspector = angular.copy(res.data);
			$modalInstance.dismiss('cancel');
		});
	}

}];