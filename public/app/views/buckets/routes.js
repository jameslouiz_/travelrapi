'use strict';

/**
 * Trip Routes 
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('app.secure.trips', {
        abstract: true,
        url: '/s',
        views: {
            '': {
                template: '<div ui-view="secure"></div>'
            }
        }
    })
    .state('app.public.trips', {
        abstract: true,
        views: {
            '': {
                template: '<div ui-view="" class="main"></div>'
            }
        }
    })
}];