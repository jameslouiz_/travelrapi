'use strict';

/**
 * Trip Inspector Controller
 */
module.exports = ['$scope', '$state', 'model', 'tripsModel', '$modalInstance', 'googleAPI', '$q', '$timeout', function ($scope, $state, model, tripsModel, $modalInstance, googleAPI, $q, $timeout) {

	$scope.inspector = angular.copy(model);

	$scope.getLocation = function (val) {
		return googleAPI.getLocation(val);
	};

	$scope.save = function (model, form) {

		if (form.$invalid) {
			return;
		}

		var tripAlreadyExists = !!model._id; // coerce to a boolean

		$scope.Promise = googleAPI.toLatLong(model.location)
			.then(function (geo) {

				model.geo = geo;

				console.log(geo);

				return tripsModel.save(model)
			})
			.then(function (res) {

				$scope.inspector = angular.copy(res.trips.active);

				$scope.tripCreated = !tripAlreadyExists;

				if (tripAlreadyExists) {
					$modalInstance.dismiss('cancel');
				}

				return $q.when($scope.inspector);

			}, function (res) {
				return $q.reject(res);
			});
	};

	$scope.destroy = function (id) {
		$scope.Promise = tripsModel.destroy(id).then(function () {
			return $modalInstance.dismiss('cancel');
		})
        .then(function () {
            $state.go('app.secure.trips.list');
        },function () {
            //$state.go('app.secure.trips.list');
        });
	};

	$scope.edit = function () {
		$modalInstance.dismiss('!^');
		$state.go('app.secure.trips.edit', {id: $scope.inspector._id});
	};

	$scope.view = function () {
		$modalInstance.dismiss('!^');
		$state.go('app.public.trips.map', {id: $scope.inspector._id});
	};

	$scope.close = function () {
		$modalInstance.dismiss('cancel');
	};
	
}];