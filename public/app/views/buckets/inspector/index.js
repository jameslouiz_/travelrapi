'use strict';

/**
 * Bucket Inspector Index
 */

module.exports = angular.module('app.buckets.inspector', [])
	.factory('bucketInspectorModal', require('./bucketInspectorModal'))
	.controller('BucketInspectorController', require('./bucketInspectorController'))
	//.config(require('./routes'));