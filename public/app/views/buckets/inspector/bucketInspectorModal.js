'use strict';

/**
 * Trip Inspector Modal Definition
 *
 */
module.exports = ['$modal', function ($modal) {
    return function (model) {
        var modalInstance = $modal.open({
            templateUrl: './app/views/bucket/inspector/bucketInspector.html',
            //template: '<div ui-view="modal"></div>'
            controller: 'BucketInspectorController',
            size: 'md',
            resolve: {
                model: function () {
                    return model;
                }
            }
        });

        return modalInstance;
    };
}];