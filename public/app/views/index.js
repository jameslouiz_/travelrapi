'use strict';

/**
 * Views Index
 */

module.exports = angular.module('app.views', [
		require('./layout').name,
		require('./trips').name,
		require('./error').name,
		require('./home').name,
		require('./photos').name,
		require('./profile').name
	])
	.config(require('./routes'));