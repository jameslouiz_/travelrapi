
'use strict';

/**
 * Pages Routes
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('app.public.home', {
        url: '/',
        views: {
            '': {
                templateUrl: './app/views/home/home.html',
                controller: 'HomeController'
            }
        },
        resolve: {
            trips: ['tripsModel', function (tripsModel) {
                return tripsModel.all();
            }]
        }
    });


    $urlRouterProvider.otherwise('/');

}];