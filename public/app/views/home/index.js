'use strict';

/**
 * Home Index
 */
module.exports = angular.module('app.views.home', [])
	.controller('HomeController', require('./homeController'))
	.config(require('./routes'));