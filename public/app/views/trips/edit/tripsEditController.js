'use strict';

/**
 * Trip Edit Controller
 */
module.exports = ['$scope', '$state', 'model', 'photosModel', function ($scope, $state, model, photosModel) {

	$scope.model = model;

	/**
	 * Handles the file upload process
	 * @return {Void}          
	 */
	$scope.$watch('file', function (newValue, oldValue) {

		if (newValue === null || newValue == 'null') {
			return;
		} 

		if (newValue === oldValue){
		    return;
		}

		if (angular.isArray($scope.file)) {
			for (var i = 0; i < $scope.file.length; i++) {
				(function (file) {

					photosModel.upload($state.params.id, file);

				})($scope.file[i]);
			}
		} else {
			photosModel.upload($state.params.id, $scope.file);
		}
	});

}];