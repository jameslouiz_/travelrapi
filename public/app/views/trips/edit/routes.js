'use strict';

/**
 * Trip Routes
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('app.secure.trips.edit', {
        url: '/trips/:id',
        views: {
            'secure': {
                controller: 'TripsEditController',
                templateUrl: './app/views/trips/edit/tripEdit.html'
            }
        },
        resolve: {
            model: ['photosModel', '$stateParams', function (photosModel, $stateParams) {
                return photosModel.getPhotosByTrip($stateParams.id);
            }]
        }
    });

}];