'use strict';

/**
 * Trip List Index
 */
module.exports = angular.module('app.trips.edit', [])
	.controller('TripsEditController', require('./tripsEditController'))
	.config(require('./routes'));