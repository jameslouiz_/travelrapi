'use strict';

/**
 * Trip Inspector Routes 
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    var modalOnEnter = function (isNew) {
        return ['$state', 'tripInspectorModal', 'sharedModel', function($state, tripInspectorModal, sharedModel) {
            var model = isNew ? {} : sharedModel.DS.trips.active;
            var modal = tripInspectorModal(model);

            modal.result.then(function(res) {
                console.log(res, res === '!^', 'ss');

                if (res === '!^') return;

                $state.go('^');
            }, 
            function(res) {
                console.log(res, res === '!^', 'dd');

                if (res === '!^') return;

                $state.go('^');
            });
        
        }]
    };


    $stateProvider
    .state('app.secure.trips.edit.tripInspector', {
        url: '/edit',
        onEnter: modalOnEnter(false)
    })
    .state('app.secure.trips.list.tripInspectorNew', {
        url: '/new',
        onEnter: modalOnEnter(true)
    })
    .state('app.secure.profile.tripInspectorNew', {
        url: '/new',
        onEnter: modalOnEnter(true)
    })
    .state('app.public.trips.map.tripInspector', {
        url: '/edit',
        data: {
            secure: true
        },
        onEnter: modalOnEnter(false)
    });

}];