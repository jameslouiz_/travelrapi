'use strict';

/**
 * Trip Inspector Index
 */

module.exports = angular.module('app.trips.inspector', [])
	.factory('tripInspectorModal', require('./tripInspectorModal'))
	.controller('TripInspectorController', require('./tripInspectorController'))
	.config(require('./routes'));