'use strict';

/**
 * Trip Routes
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('app.public.trips.view', {
        url: '/trip/:id',
        views: {
            '': {
                controller: 'TripsViewController',
                templateUrl: './app/views/trips/view/trip.view.html'
            }
        },
        resolve: {
            model: ['photosModel', '$stateParams', function (photosModel, $stateParams) {
                return photosModel.getPhotosByTrip($stateParams.id);
            }]
        }
    });

}];