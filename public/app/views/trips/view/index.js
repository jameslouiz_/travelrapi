'use strict';

/**
 * Trip View Index
 */
module.exports = angular.module('app.trips.view', [])
	.controller('TripsViewController', require('./tripsViewController'))
	.config(require('./routes'));