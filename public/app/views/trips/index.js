'use strict';

/**
 * Trip Index
 */
module.exports = angular.module('app.views.trips', [
		require('./list').name,
		require('./edit').name,
		require('./map').name,
		require('./view').name,
		require('./inspector').name
	])
	.config(require('./routes'));