'use strict';

/**
 * Trip Routes
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('app.secure.trips.list', {
        url: '/trips',
        views: {
            'secure': {
                controller: 'TripsListController',
                templateUrl: './app/views/trips/list/tripList.html'
            }
        },
        resolve: {
            trips: ['tripsModel', function (tripsModel) {
                return tripsModel.getTripsByUser('me');
            }]
        }
    });

}];