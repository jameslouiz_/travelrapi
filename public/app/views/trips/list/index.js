'use strict';

/**
 * Trip List Index
 */

module.exports = angular.module('app.trips.list', [])
	.controller('TripsListController', require('./tripsListController'))
	.config(require('./routes'));