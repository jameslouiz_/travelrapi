'use strict';

/**
 * Trip List Controller
 */
module.exports = ['$scope', '$state', 'tripsModel', 'trips', function ($scope, $state, tripsModel, trips) {

	$scope.tripsModel = tripsModel;

	$scope.trips = trips;

}];