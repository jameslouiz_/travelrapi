'use strict';

/**
 * Trip List Index
 */

module.exports = angular.module('app.trips.map', [])
	.controller('TripsMapController', require('./tripsMapController'))
	.config(require('./routes'));