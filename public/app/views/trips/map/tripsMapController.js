'use strict';

/**
 * Trip Map Controller
 */
module.exports = ['$scope', '$state', 'photosModel', 'model', 'tpMapFactory', function ($scope, $state, photosModel, model, tpMapFactory) {

	$scope.model = model;

	tpMapFactory.mapInit = function () {
		tpMapFactory.removeAll();
		tpMapFactory.addCollection(model.photos.collection);
	};

	tpMapFactory.$marker.onClick = function (marker) {
		$state.go('app.public.trips.map.photo.view', {photoId: marker.uid});
	};

}];