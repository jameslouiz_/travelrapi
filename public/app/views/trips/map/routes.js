'use strict';

/**
 * Trip Routes
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('app.public.trips.map', {
        url: '/map/:id',
        views: {
            '': {
                controller: 'TripsMapController',
                templateUrl: './app/views/trips/map/tripMap.html'
            }
        },
        resolve: {
            model: ['photosModel', '$stateParams', function (photosModel, $stateParams) {
                return photosModel.getPhotosByTrip($stateParams.id);
            }]
        }
    });

}];