'use strict';

/**
 * Error Index
 */

module.exports = angular.module('app.views.error', [])
	.config(require('./routes'));