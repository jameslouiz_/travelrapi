
'use strict';

/**
 * Pages Routes
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('app.public.error', {
        abstract: true,
        url: '/error',
        views: {
            '': {
                template: '<div ui-view="error"></div>'
            }
        }
    })
    .state('app.public.error.status', {
        url: '/:statusCode',
        views: {
            'error': {
                controller: ['$state', '$scope', function ($state, $scope) {
                    $scope.status = $state.params.statusCode;
                }],
                templateUrl: './app/views/error/error.html'
            }
        }
    })


    $urlRouterProvider.otherwise('/');

}];