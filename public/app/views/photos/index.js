'use strict';

/**
 * Photos Index
 */

module.exports = angular.module('app.views.photos', [
		require('./inspector').name,
		require('./viewer').name
	]);