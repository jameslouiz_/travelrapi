'use strict';

/**
 * Photo Inspector controller
 */
module.exports = ['$scope', '$modalInstance', '$photosAPI', '$weatherAPI', '$map', '$state', '$geocode',
    function ($scope, $modalInstance, $photosAPI, $weatherAPI, $map, $state, $geocode) {

    /**
     * Flag to make message available to scope
     * @type {String}
     */
    $scope.photosAPI = $photosAPI;

    $photosAPI.index = $photosAPI.getIndex($state.params.photo);

    console.log($photosAPI.index, $photosAPI.idMap, $state.params.photo);
    
    var showNextControls = function () {
        return $photosAPI.index < $photosAPI.photos.length - 1;
    };
    $scope.showNextControls = showNextControls();

    var showPrevControls = function () {
        return $photosAPI.index > 0;
    };
    $scope.showPrevControls = showPrevControls();

    var showControls = function () {
        $scope.showNextControls = showNextControls();
        $scope.showPrevControls = showPrevControls();
    };

    var updateForm = function () {
        $scope.inspector = $photosAPI.photos[$photosAPI.index];
    };
    updateForm();

    $scope.next = function () {
        $photosAPI.index = $photosAPI.index < $photosAPI.photos.length - 1 ? $photosAPI.index + 1 : $photosAPI.photos.length - 1;
        $state.go('app.secure.trips.single.photo.view', {photo: $photosAPI.getFromCacheAtIndex($photosAPI.index)._id})
        showControls();
        updateForm();
    };

    $scope.prev = function () {
        $photosAPI.index = $photosAPI.index > 0 ? $photosAPI.index - 1 : 0;
        $state.go('app.secure.trips.single.photo.view', {photo: $photosAPI.getFromCacheAtIndex($photosAPI.index)._id})
        showControls();
        updateForm();
    };

    $scope.save = function (model) {
        $photosAPI.update(model)
            .then(function (res) {
                console.log(res);
            }, function (e) {
                console.log(e)
            });
    };

    $scope.destroy = function (id) {
        console.log(id, 'id');
        $photosAPI.destroy(id)
            .then(function () {
                $modalInstance.dismiss('cancel');
            })
    };

    /**
     * Close Modal
     */
    $scope.close = function () {
        $modalInstance.dismiss('cancel');

        if (prompt.dismiss === undefined) {
            return;
        }

        if (typeof prompt.dismiss !== 'function') {
            throw 'callback is not a function dismiss'
        }

        prompt.dismiss();
    };


}];