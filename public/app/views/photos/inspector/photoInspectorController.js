'use strict';

/**
 * Photo Inspector controller
 */
module.exports = ['$scope', '$state', 'photo', 'photosModel', 'sharedModel', '$modalStack',
    function ($scope, $state, photo, photosModel, sharedModel, $modalStack) {

    $scope.photos = photo;

    $scope.index = sharedModel.getIndexFromCollectionById(photo.photos.active._id, 'photos');

    console.log($scope.index, $state);

    $scope.save = function (model) {
        $scope.Promise = photosModel.update(model).then(function (res) {
            console.log(res, 'res');

            $modalStack.dismissAll();
        });
    };

    $scope.destroy = function (id) {
        $scope.Promise = photosModel.destroy(id).then(function () {
            return $modalStack.dismissAll();
        });
    };

    $scope.next = function () {

        var nextId,
            total = photo.photos.collection.length;

        console.log(photo.photos.active.index);

        if ($scope.index < total - 1) {
            nextId = photo.photos.collection[$scope.index + 1]._id,
            $state.go('app.secure.trips.edit.photo.view', {photoId: nextId});
        };
    };

    $scope.prev = function () {

        var prevId,
            total = photo.photos.collection.length;

        if ($scope.index > 0) {
            prevId = photo.photos.collection[$scope.index - 1]._id,
            $state.go('app.secure.trips.edit.photo.view', {photoId: prevId});
        }
    };

    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };

}];