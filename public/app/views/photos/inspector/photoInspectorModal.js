'use strict';

/**
 * Photo Inspector Modal Definition
 *
 */
module.exports = ['$modal', function ($modal) {
    return function (model) {
        var modalInstance = $modal.open({
            templateUrl: './app/views/photos/inspector/photoInspector.html',
            //template: '<div ui-view="modal"></div>'
            size: 'lg',
            windowClass: 'modal-fit-to-screen',
            resolve: {
                model: function () {
                    return model;
                }
            },
            controller: ['$scope', 'sharedModel', function ($scope, sharedModel) {
                $scope.photos = sharedModel.DS;
            }]
        });

        return modalInstance;
    };
}];