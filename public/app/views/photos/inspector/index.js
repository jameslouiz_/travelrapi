'use strict';

/**
 * Photo Inspector Index
 */

module.exports = angular.module('app.views.photos.inspector', [])
	.controller('PhotoInspectorController', require('./photoInspectorController'))
	.factory('photoInspectorModal', require('./photoInspectorModal'))
	.config(require('./routes'));