'use strict';

/**
 * Photo Routes
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('app.secure.trips.edit.photo', {
        url: '/p',
        abstract: true,
        onEnter: ['$stateParams', '$state', 'photoInspectorModal', function($stateParams, $state, photoInspectorModal) {
            var modal = photoInspectorModal({})

            modal.result.finally(function() {
                $state.go('^.^');
            }, function() {
                $state.go('^.^');
            })

        }]
    })
    .state('app.secure.trips.edit.photo.view', {
        url: '/:photoId',
        views: {
            'modal@': {
                resolve: {
                    photo: ['$stateParams', 'photosModel', function ($stateParams, photosModel) {
                        return photosModel.getPhotoById($stateParams.photoId);
                    }]
                },
                controller: 'PhotoInspectorController',
                templateUrl: './app/views/photos/inspector/photoInspectorChild.html'
            }
        }
    })

}];