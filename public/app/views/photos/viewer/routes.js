'use strict';

/**
 * Photo Routes
 */
module.exports = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('app.public.trips.map.photo', {
        url: '/p',
        abstract: true,
        onEnter: ['$stateParams', '$state', 'photoViewerModal', function($stateParams, $state, photoViewerModal) {
            var modal = photoViewerModal({})

            modal.result.finally(function() {
                $state.go('^.^');
            }, function() {
                $state.go('^.^');
            })

        }]
    })
    .state('app.public.trips.map.photo.view', {
        url: '/:photoId',
        views: {
            'modal@': {
                resolve: {
                    photo: ['$stateParams', 'photosModel', function ($stateParams, photosModel) {
                        return photosModel.getPhotoById($stateParams.photoId);
                    }]
                },
                controller: 'PhotoViewerController',
                templateUrl: './app/views/photos/viewer/photoViewerChild.html'
            }
        }
    })

}];