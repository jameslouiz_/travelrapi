'use strict';

/**
 * Photo Viewer Index
 */

module.exports = angular.module('app.views.photos.viewer', [])
	.controller('PhotoViewerController', require('./photoViewerController'))
	.factory('photoViewerModal', require('./photoViewerModal'))
	.config(require('./routes'));