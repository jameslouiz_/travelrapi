'use strict';

/**
 * Photo Inspector Modal Definition
 *
 */
module.exports = ['$modal', function ($modal) {
    return function (model) {
        var modalInstance = $modal.open({
            templateUrl: './app/views/photos/viewer/photoViewer.html',
            //template: '<div ui-view="modal"></div>'
            size: 'lg',
            windowClass: 'modal-fit-to-screen',
            resolve: {
                model: function () {
                    return model;
                }
            },
            controller: ['$scope', 'photosModel', '$modalInstance', function ($scope, photosModel, $modalInstance) {
                $scope.photos = photosModel.DS;

                $scope.close = function () {
                    $modalInstance.dismiss('cancel');
                };
            }]
        });

        return modalInstance;
    };
}];