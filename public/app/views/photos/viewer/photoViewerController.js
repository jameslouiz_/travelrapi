'use strict';

/**
 * Photo Viewer controller
 */
module.exports = ['$scope', '$state', 'photo', 'photosModel', 'sharedModel', '$modalStack',
    function ($scope, $state, photo, photosModel, sharedModel, $modalStack) {

    $scope.photo = photo;

    $scope.index = sharedModel.getIndexFromCollectionById(photo.photos.active._id, 'photos');

    $scope.save = function (model) {
        photosModel.update(model).then(function () {
            $modalStack.dismissAll();
        });
    };

    $scope.destroy = function (id) {
        photosModel.destroy(id).then(function () {
            $modalStack.dismissAll();
        });
    };

    $scope.next = function () {

        var nextId,
            total = photo.photos.collection.length;

        if ($scope.index < total - 1) {
            nextId = photo.photos.collection[$scope.index + 1]._id,
            $state.go('app.public.trips.map.photo.view', {photoId: nextId});
        };
    };

    $scope.prev = function () {

        var prevId,
            total = photo.photos.collection.length;

        if ($scope.index > 0) {
            prevId = photo.photos.collection[$scope.index - 1]._id,
            $state.go('app.public.trips.map.photo.view', {photoId: prevId});
        }
    };

    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };

}];