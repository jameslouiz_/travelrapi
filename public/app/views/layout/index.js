'use strict';

/**
 * Layout Index
 */

module.exports = angular.module('app.views.layout', [
		require('./header').name
	]);