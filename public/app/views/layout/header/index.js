'use strict';

/**
 * Header Index
 */

module.exports = angular.module('app.views.layout.header', [])
	.controller('HeaderController', require('./headerController'));