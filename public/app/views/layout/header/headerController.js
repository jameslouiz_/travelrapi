'use strict';

/**
 * Header Controller
 */
module.exports = ['$scope', 'userModal', 'userModel', 'tripsModel', '$state', function ($scope, userModal, userModel, tripsModel, $state) {

	$scope.logout = function () {
		userModel.logout().then(function () {
			if ($state.includes('app.secure') || tripsModel.DS.active.privacy !== 1) {
				$state.go('app.public.home');
			}
		})
	};

	$scope.authenticate = function () {
		userModal.authenticate();
	};

}];