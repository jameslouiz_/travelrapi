module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        app: {
			// configurable paths
			app: '/',
			dist: 'dist'
	    },

        watch: {
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '/{,*/}*.html'
                ]
            }
        },
        connect: {
            options: {
                port: 3001,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    open: true,
                    base: [
                        '.tmp',
                        './'
                    ]
                }
            }
        },
        browserify: {
        	js: {
		        src: './app/app.js',
		        dest: './app/app.min.js'
	    	},
            options: {
                transform: ['debowerify']
            }
        },
        uglify: {
            task: {
                options: {
                    mangle: false
                },
                files: {
                    './Cloud/public/app/app.min.js': ['./Cloud/public/app/app.min.js']
                }
            }
        },
        copy: {
            main: {
                files: [
                    // includes files within path and its sub-directories
                    {src: ['./app/**'], dest: './Cloud/public/', expand: true},
                    {src: ['./assets/**'], dest: './Cloud/public/', expand: true},
                    {src: ['./index.html'], dest: './Cloud/public/', expand: true},
                    {src: ['./cache.appcache'], dest: './Cloud/public/', expand: true}
                ],
            },
        },
    });


    // Load Grunt tasks
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-livereload');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');


    // Default task(s).
    grunt.registerTask('deploy', ['browserify', 'copy', 'uglify',]);
    grunt.registerTask('default', ['connect', 'watch']);
    grunt.registerTask('js', ['browserify']);

};