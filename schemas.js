module.exports = function (db) {

	var timestamps = require('mongoose-timestamp');
	var deepPopulate = require('mongoose-deep-populate')(db);

	var module = {};

	module.trip = function () {
		
		var TripSchema = new db.Schema({
			thumbnail: {
				type: db.Schema.Types.ObjectId,
				ref: 'Image'
			},
			user: {
				type: db.Schema.Types.ObjectId,
				ref: 'User'
			},
			photos: [
				{
					type: db.Schema.Types.ObjectId,
					ref: 'Image'
				}
			],
			photoCount: {
				type: Number
			}
		}, {strict: false});

		TripSchema.plugin(timestamps);
		TripSchema.plugin(deepPopulate);

		return TripSchema;
	
	};

	module.image = function () {
		
		var ImageSchema = new db.Schema({
			user: { 
				type: db.Schema.Types.ObjectId, 
				ref: 'Trip' 
			}
		}, {strict: false});

		ImageSchema.plugin(timestamps);

		return ImageSchema;
	
	};

	module.user = function () {
		
		var UserSchema = new db.Schema({
			thumbnail: {
				type: db.Schema.Types.ObjectId,
				ref: 'Image'
			},
			cover: {
				type: db.Schema.Types.ObjectId,
				ref: 'Image'
			}
		}, {strict: false});

		UserSchema.plugin(timestamps);

		return UserSchema;
	}

	return module;

}