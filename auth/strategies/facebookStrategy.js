var FacebookStrategy = require('passport-facebook').Strategy;

module.exports = function (user, passport) {

	var User = user;

	passport.use(new FacebookStrategy({
		clientID: '1764639813790800',
		clientSecret: '6131477b5081a65110ee6730845c1668',
		callbackURL: "http://localhost:3000/users/login/facebook/callback",
		enableProof: false,
		successRedirect: '/login/facebook/success',
		profileFields: ['id', 'displayName', 'emails', 'last_name', 'middle_name', 'first_name', 'friends', 'picture']
	},
		function(accessToken, refreshToken, profile, done) {

			User.findOne({'facebook._id': profile.id})
				.exec(function (err, user) {
					if (err) {
						return done(err);
					}

					console.log(profile);

					if (!user) {
						User.create({
							name: profile.displayName,
							email: profile.emails[0].value,
							facebook: {
								_id: profile.id
							}
						}, function (err, user) {
							if (err) {
								return done(err);
							}

							if (user) {
								return done(null, user);
							}
						})
					}

					return done(null, user);
				})
		}
	));


	// Configure Passport authenticated session persistence.
	//
	// In order to restore authentication state across HTTP requests, Passport needs
	// to serialize users into and deserialize users out of the session.  The
	// typical implementation of this is as simple as supplying the user ID when
	// serializing, and querying the user record by ID from the database when
	// deserializing.



	return passport;
};