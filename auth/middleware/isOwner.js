
module.exports = function (collectionName) {


    return function(req, res, next) {

        req.cursor = collectionName.findById(req.params.id, function (err, doc) {

            if (err) {
                return res.status(500).send(err);
            }

            if (!doc) {
                return res.send({});
            }

            if (!req.isAuthenticated()) {
                console.log('not auth')
                return res.status(401).send({});
            }

            if (doc.userId.toString() !== req.user._id.toString()) {
                return res.status(403).send({message: 'Not authorized to modify object'});
            }

            next();
        });
    };

};