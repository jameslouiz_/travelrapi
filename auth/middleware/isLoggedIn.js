module.exports = function(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (!req.isAuthenticated()) {
        return res.status(401).send({message: 'User is not authenticated'});
    }

    //delete req.user.password;

    // if they aren't redirect them to the home page
    next();
};