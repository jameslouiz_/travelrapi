
module.exports = function (trip) {

    function isOwner (doc, user) {
        var user = user || {};
        return doc.userId.toString() !== user._id.toString()
    };

    function isFriend (doc, user) {
        return false;
    };

    return function(req, res, next) {

        req.cursor = trip.findById(req.params.id, function (err, doc) {

            if (!doc) {
                return res.send({});
            }

            var privacy = doc.privacy;

            if (err) {
                return res.status(500).send(err);
            }

            if (privacy === 1) {
                return next();
            }

            // If we got this far it means the document is not
            // public so we need to log in
            if (!req.isAuthenticated()) {
                return res.status(401).send();
            }

            console.log(4);

            // This is private and only creater can view it
            if (privacy === 2 && !isOwner(doc, req.user)) {
                return res.status(403).send({
                    message: 'not owner'
                });
            }

            console.log(5);

            // This is only visible by friends or the owner itself
            if (privacy === 3 && (!isOwner(doc, req.user) || !isFriend(doc, req.user))) {
                return res.status(403).send({
                    message: 'only friends can view'
                });
            }

            console.log(6);
            
            next();

        }).lean();
    };

};