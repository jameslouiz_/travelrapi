var LocalStrategy = require('passport-local').Strategy;

module.exports = function (passport, db) {
	passport.use('local-signup', new LocalStrategy(
		{
		    usernameField: 'email',
		    passwordField: 'password'
		},
		function(email, password, done) {
			var Users = db.get('Users');
			
			Users.findOne({
				email: email
			}, function(err, user) {
				if (err) { 
					return done(err); 
				}

				if (!user) { 
					return done(null, false, {message: 'User already exists'}); 
				} else {
					return done(null, user);
				}

			});

		}));

	passport.use('local', new LocalStrategy(
		{
		    usernameField: 'email',
		    passwordField: 'password',
		    passReqToCallback : true
		},
		function(req, email, password, done) {
			var Users = db.get('Users');

			Users.findOne({ email: email }, function (err, user) {
				if (err) { 
					return done(err); 
				}

				if (!user) { 
					return done(null, false); 
				}

				if (user.password !== password) { 
					return done(null, false); 
				}

				return done(null, user);
			});

		}));


	// Configure Passport authenticated session persistence.
	//
	// In order to restore authentication state across HTTP requests, Passport needs
	// to serialize users into and deserialize users out of the session.  The
	// typical implementation of this is as simple as supplying the user ID when
	// serializing, and querying the user record by ID from the database when
	// deserializing.
	passport.serializeUser(function(user, cb) {
		console.log('serializing');
	  	cb(null, user._id);
	});

	passport.deserializeUser(function(id, cb) {
		var Users = db.get('Users');

		console.log('deserializing', id);

	  	Users.findOne({_id: id}, function (err, user) {
			if (err) { 
				return cb(err); 
			}
			cb(null, user);
		});
	});


	return passport;
};